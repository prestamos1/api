<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('empresas')->insert([
            'documento' => '005078088',
            'razon_social' => 'ac soluciones digitales',
            'telefono' => '935701981',
            'correo' => 'fhttvchgtv',
            'logo' => 'no',
            'web' => 'armandocampos.com'
        ]);

    }
}
