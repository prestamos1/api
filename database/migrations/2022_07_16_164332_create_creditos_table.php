<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creditos', function (Blueprint $table) {
            $table->id();
            $table->integer('idCliente');
            $table->date('fecha_generacion');
            $table->date('primera_couta');
            $table->date('ultima_couta');
            $table->float('monto');
            $table->float('inicial');
            $table->float('credito');
            $table->integer('interes');
            $table->integer('coutas');
            $table->integer('intervalo');
            $table->integer('estadoCredito');
            $table->integer('idFactura');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creditos');
    }
};
