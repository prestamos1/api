<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->id();
            $table->dateTime('fecha');
            $table->integer('idCliente');
            $table->string('numero_documento');
            $table->float('subtotal');
            $table->float('igv');
            $table->float('total');
            $table->integer('tipoPago');
            $table->string('observacionPago', 3000)->nullable();
            $table->integer('estadoFactura');
            $table->integer('idCredito')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
};
