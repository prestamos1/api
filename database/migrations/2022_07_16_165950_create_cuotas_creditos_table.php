<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuotas_creditos', function (Blueprint $table) {
            $table->id();
            $table->integer('idCredito');
            $table->integer('idCliente');
            $table->float('monto');
            $table->float('interes');
            $table->float('total');
            $table->date('fecha_couta');
            $table->date('fecha_pago')->nullable();
            $table->integer('estadoCuota')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuotas_creditos');
    }
};
