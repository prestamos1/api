<?php

namespace App\Models;

use App\Http\Controllers\CreditoController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    protected $fillable = [
        'tipo_documento',
        'documento',
        'nombres',
        'apellidos',
        'direccion',
        'correo',
        'telefono',
        'observaciones',
        'estadoCliente',
    ];

    public function creditos()
    {
        return $this->hasMany(Credito::class, 'id', 'idCliente');
    }
}
