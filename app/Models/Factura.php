<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    use HasFactory;

    protected $fillable = [
        'fecha',
        'idCliente',
        'numero_documento',
        'subtotal',
        'igv',
        'total',
        'tipoPago',
        'observacionPago',
        'estadoFactura',
    ];

    public function cliente()
    {
        return $this->hasOne(Cliente::class, 'id', 'idCliente');
    }

    public function relacion()
    {
        return $this->hasMany(RelacionFacturaProducto::class, 'idFactura', 'id');
    }
}
