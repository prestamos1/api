<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CuotasCredito extends Model
{
    use HasFactory;

    protected $fillable = [
        'idCredito',
        'idCliente',
        'monto',
        'interes',
        'total',
        'fecha_couta',
    ];

    public function credito()
    {
        return $this->hasOne(Credito::class, 'id', 'idCredito');
    }
}
