<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Credito extends Model
{
    use HasFactory;

    protected $fillable = [
        'idCliente',
        'fecha_generacion',
        'primera_couta',
        'ultima_couta',
        'monto',
        'inicial',
        'credito',
        'interes',
        'coutas',
        'intervalo',
        'estadoCredito',
        'idFactura'
    ];

    public function cliente()
    {
        return $this->hasOne(Cliente::class, 'id', 'idCliente');
    }

    public function factura()
    {
        return $this->hasOne(Factura::class, 'id', 'idFactura');
    }

    public function pagadas()
    {
        return $this->hasMany(CuotasCredito::class, 'idCredito', 'id')->where('estadoCuota', 2);
    }

    public function sinpagar()
    {
        return $this->hasMany(CuotasCredito::class, 'idCredito', 'id')->where('estadoCuota', 1);
    }

    public function mora()
    {
        return $this->hasMany(CuotasCredito::class, 'idCredito', 'id')->where('estadoCuota', 3);
    }
}
