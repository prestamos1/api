<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RelacionFacturaProducto extends Model
{
    use HasFactory;

    protected $fillable = [
        'idFactura',
        'idProducto',
        'cantidad',
        'precio',
    ];

    public function producto()
    {
        return $this->hasOne(Producto::class, 'id', 'idProducto');
    }
}
