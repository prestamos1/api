<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $user = User::all();
        return $user;
    }

    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->user,
            'password' => Hash::make($request->password),
            'typeUser' => $request->type_user,
            'statusUser' => 1
        ]);

        return response()->json([
            'status' => true
        ]);
    }

    public function update(Request $request)
    {
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->user;
        $user->typeUser = $request->type_user;
        $user->save();

        return response()->json([
            'status' => true
        ]);
    }

    public function getUser(User $user)
    {
        return $user;
    }

    public function destroy(User $user)
    {
        $user->delete();

        return response()->json([
            'status' => true
        ]);
    }

    public function password(Request $request)
    {
        $user = User::find($request->id);
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            'status' => true
        ]);
    }
}
