<?php

namespace App\Http\Controllers;

use App\Models\{Credito, CuotasCredito, Cliente};
use Illuminate\Http\Request;

class CuotasCreditoController extends Controller
{

    public function index($cuota)
    {
        $cuotad = CuotasCredito::find($cuota);
        $credito = Credito::find($cuotad->idCredito);
        $cliente = Cliente::find($cuotad->idCliente);
        return response()->json([
            'cuota' => $cuotad,
            'credito' => $credito,
            'cliente' => $cliente
        ]);
    }

    public function update(Request $request)
    {
        $cuota = CuotasCredito::find($request->idCuota);
        $cuota->estadoCuota = 2;
        $cuota->fecha_pago = date('Y-m-d');
        $cuota->save();

        $cuotasSP = CuotasCredito::where('estadoCuota', 3)->where('idCliente', $cuota->idCliente)->count();

        if($cuotasSP == 0)
        {
            $credito = Credito::find($cuota->idCredito);
            $credito->estadoCredito = 2;
            $credito->save();
        }

        return response()->json([
            'status' => true
        ]);
    }

}
