<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Credito;
use App\Models\CuotasCredito;
use App\Models\Factura;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index()
    {
        return Cliente::all();
    }

    public function store(Request $request)
    {
        $cliente = Cliente::create([
            'tipo_documento' => $request->tipo_documento,
            'documento' => $request->documento,
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'direccion' => $request->direccion,
            'correo' => $request->correo,
            'telefono' => $request->telefono,
            'observaciones' => $request->observaciones,
            'estadoCliente' => 1
        ]);

        return response()->json([
            'status' => true
        ]);
    }


    public function show(Cliente $cliente)
    {
        return $cliente;
    }


    public function update(Request $request)
    {
        $cliente = Cliente::find($request->id);
        $cliente->tipo_documento = $request->tipo_documento;
        $cliente->documento = $request->documento;
        $cliente->nombres = $request->nombres;
        $cliente->apellidos = $request->apellidos;
        $cliente->direccion = $request->direccion;
        $cliente->correo = $request->correo;
        $cliente->telefono = $request->telefono;
        $cliente->observaciones = $request->observaciones;
        $cliente->estadoCliente = $request->estadoCliente;
        $cliente->save();

        return response()->json([
            'status' => true
        ]);
    }


    public function destroy(Cliente $cliente)
    {
        $cliente->delete();

        return response()->json([
            'status' => true
        ]);
    }

    public function validar($documento)
    {
        $user = Cliente::where('documento', $documento)->count();

        if($user > 0)
        {
            return 'existe';
        }
        else{
            return 0;
        }
    }

    public function detalle($cliente)
    {
        $data = Cliente::find($cliente);
        $creditos = Credito::where('idCliente', $cliente)->get();
        $cuotas = CuotasCredito::where('idCliente', $cliente)->with('credito')->whereIn('estadoCuota', [1, 3])->orderBy('fecha_couta')->limit(7)->get();
        $cuotasm = CuotasCredito::where('estadoCuota', 1)->where('idCliente', $cliente)->whereDate('fecha_couta', '<', date('Y-m-d'))->get();

        foreach ($cuotasm as $cuota) {
            $cuota->estadoCuota = 3;
            $cuota->save();
        }
        $cuotasm = CuotasCredito::where('estadoCuota', 3)->where('idCliente', $cliente)->with('credito')->get();

        $pagadas = CuotasCredito::where('idCliente', $cliente)->where('estadoCuota', 2)->count();
        $sinpagar = CuotasCredito::where('idCliente', $cliente)->where('estadoCuota', 1)->count();
        $vencidas = CuotasCredito::where('estadoCuota', 3)->where('idCliente', $cliente)->count();
        $facturas = Factura::where('idCliente', $cliente)->get();
        $historial = CuotasCredito::where('idCliente', $cliente)->where('estadoCuota', 2)->get();
        return response()->json([
            'cliente' => $data,
            'cuotas' => $cuotas,
            'coutas_mora' => $cuotasm,
            'creditos' => $creditos,
            'pagadas' => $pagadas,
            'sinpagar' => $sinpagar,
            'mora' => $vencidas,
            'facturas' =>  $facturas,
            'totalCuotas' => $pagadas + $sinpagar + $vencidas,
            'historia_pagos' => $historial
        ]);
    }
}
