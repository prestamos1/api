<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCreditoRequest;
use App\Http\Requests\UpdateCreditoRequest;
use App\Models\Cliente;
use App\Models\Credito;
use App\Models\CuotasCredito;
use App\Models\Factura;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CreditoController extends Controller
{

    public function index()
    {
        return Credito::with('cliente', 'factura')->withCount('pagadas', 'sinpagar', 'mora')->get();
    }

    public function detalle($credito)
    {
        $credit = Credito::find($credito);
        return response()->json([
            'credito' => $credit,
            'cliente' => Cliente::find($credit->idCliente),
            'cuotas' => CuotasCredito::where('idCredito', $credito)->with('credito')->get()
        ]);
    }

    public function simular(Request $request)
    {
        $monto_cuota = $request->credito / $request->cuotas;
        $interes_couta = $monto_cuota / 100 * $request->interes;

        $coutas = array();
        $fecha = Carbon::parse($request->fecha);
        $totalInteres = 0;
        $total = 0;
        $totalCuotas = 0;

        for ($i=0; $i < $request->cuotas; $i++) {
            $couta = array(
                'monto' => number_format($monto_cuota, 2, '.', ''),
                'interes' => number_format($interes_couta, 2, '.', ''),
                'total' => number_format($monto_cuota + $interes_couta, 2, '.', ''),
                'fecha_pago' => $fecha->format('Y-m-d')
            );
            $valor = $request->intervalos;
            if ($valor == 30) {
                $fecha = $fecha->addMonth();
            } else {
                $fecha = $fecha->addDay($valor);
            }

            $total = $total + $couta['total'];
            $totalCuotas = $totalCuotas + $couta['monto'];
            $totalInteres = $totalInteres + $couta['interes'];

            array_push($coutas, $couta);
        }

        return response()->json([
            'coutas' => $coutas,
            'totalCuotas' => number_format($totalCuotas, 2, '.', ''),
            'totalInteres' => number_format($totalInteres, 2, '.', ''),
            'total' => number_format($total, 2, '.', '')
        ]);
    }


    public function generar(Request $request)
    {

        $credito = Credito::create([
            'idCliente' => $request->idCliente,
            'fecha_generacion' => date('Y-m-d'),
            'primera_couta' => $request->fecha,
            'ultima_couta' => $request->fecha,
            'monto' => number_format($request->monto, 2, '.', ''),
            'inicial' => number_format($request->inicial, 2, '.', ''),
            'credito' => number_format($request->credito, 2, '.', ''),
            'interes' => $request->interes,
            'coutas' => $request->cuotas,
            'intervalo' => $request->intervalos,
            'estadoCredito' => 1,
            'idFactura' => $request->idFactura
        ]);

        if($request->idFactura != ''){
            $factura = Factura::find($request->idFactura);
        $factura->idCredito = $credito->id;
        $factura->save();
        }
        


        $monto_cuota = $request->credito / $request->cuotas;
        $interes_couta = $monto_cuota / 100 * $request->interes;

        $fecha = Carbon::parse($request->fecha);

        for ($i = 0; $i < $request->cuotas; $i++) {

            $coutas = CuotasCredito::create([
                'idCredito' => $credito->id,
                'idCliente' => $request->idCliente,
                'monto' => number_format($monto_cuota, 2, '.', ''),
                'interes' => number_format($interes_couta, 2, '.', ''),
                'total' => number_format($monto_cuota + $interes_couta, 2, '.', ''),
                'fecha_couta' => $fecha->format('Y-m-d')
            ]);

            $valor = $request->intervalos;
            if($valor == 30)
            {
                $fecha = $fecha->addMonth();
            }
            else{
                $fecha = $fecha->addDay($valor);
            }

        }

        $credito->ultima_couta = $fecha;
        $credito->save();

        return response()->json([
            'status' => true
        ]);
    }
}
