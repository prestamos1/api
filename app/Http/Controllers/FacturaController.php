<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFacturaRequest;
use App\Http\Requests\UpdateFacturaRequest;
use App\Models\Cliente;
use App\Models\Factura;
use App\Models\RelacionFacturaProducto;
use Illuminate\Http\Request;

class FacturaController extends Controller
{
    public function index()
    {
        return Factura::where('estadoFactura', '>', 0)->with('cliente', 'relacion', 'relacion.producto')->get();
    }

    public function facturas($cliente)
    {
        return Factura::where('estadoFactura', '>', 0)->where('idCliente', $cliente)->get();
    }


    public function store(Request $request)
    {
        $total = Factura::where('estadoFactura', '>', 0)->count();
        if($total < 10){ $correlativo = "000".$total; }
        if($total >= 10 && $total <= 99){ $correlativo = "00".$total; }
        if($total > 99 && $total <= 999){ $correlativo = "0".$total; }
        if($total > 999 && $total <= 9999){ $correlativo = $total; }

        if($correlativo == 0000){ $correlativo = "0001"; }

        $factura = Factura::create([
            'fecha' => date('Y-m-d H:i:s'),
            'idCliente' => $request->idCliente,
            'numero_documento' => 'F001-'.$correlativo,
            'subtotal' => 0,
            'igv' => 0,
            'total' => 0,
            'tipoPago' => 0,
            'estadoFactura' => 0
        ]);
        $detalle = Factura::whereId($factura->id)->with('cliente')->first();
        return $detalle;
    }

    public function show($factura)
    {
        $facturaa = Factura::whereId($factura)->first();
        return response()->json([
            'factura' => $facturaa,
            'cliente' => Cliente::find($facturaa->idCliente),
            'relacion' => RelacionFacturaProducto::where('idFactura', $facturaa->id)->with('producto')->get()
        ]);
    }

    public function final(Request $request)
    {
        $factura = Factura::find($request->idFactura);
        $factura->observacionPago = $request->observaciones;
        $factura->tipoPago = $request->tipoPago;

        $relaciones = RelacionFacturaProducto::where('idFactura', $request->idFactura)->get();

        $total = 0;

        foreach ($relaciones as $relacion) {
            $var = $relacion->cantidad * $relacion->precio;
            $total = $total + $var;
        }

        $igv = $total / 100 * 18;

        $factura->igv = number_format($igv, 2, '.', '');
        $factura->total = number_format($total, 2, '.', '');
        $factura->subtotal = number_format($total-$igv, 2, '.', '');
        $factura->estadoFactura = 1;
        $factura->save();

        return response()->json([
            'status' => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFacturaRequest  $request
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFacturaRequest $request, Factura $factura)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function destroy(Factura $factura)
    {
        //
    }
}
