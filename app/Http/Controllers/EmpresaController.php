<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEmpresaRequest;
use App\Http\Requests\UpdateEmpresaRequest;
use App\Models\Empresa;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $empresa = Empresa::first();

        return $empresa;
    }

    public function update(Request $request)
    {
        $empresa = Empresa::first();
        $empresa->razon_social = $request->razon_social;
        $empresa->documento = $request->documento;
        $empresa->telefono = $request->telefono;
        $empresa->correo = $request->correo;
        $empresa->web = $request->web;
        $empresa->save();

        return response()->json([
            'status' => true
        ]);
    }

}
