<?php

namespace App\Http\Controllers;

use App\Models\RelacionFacturaProducto;
use Illuminate\Http\Request;

class RelacionFacturaProductoController extends Controller
{

    public function store(Request $request)
    {
        $new = RelacionFacturaProducto::create([
            'idFactura' => $request->idFactura,
            'idProducto' => $request->idProducto,
            'cantidad' => $request->cantidad,
            'precio' => $request->precio
        ]);

        $relaciones = RelacionFacturaProducto::where('idFactura', $request->idFactura)->with('producto')->get();

        $total = 0;

        foreach ($relaciones as $relacion ) {
            $var = $relacion->cantidad * $relacion->precio;
            $total = $total + $var;
        }

        $igv = $total / 100 * 18;

        return response()->json([
            'data' => $relaciones,
            'total' => $total,
            'igv' => $igv,
            'subTotal' => $total - $igv
        ]);
    }

}
