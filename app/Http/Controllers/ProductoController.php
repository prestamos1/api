<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    public function index()
    {
        return Producto::all();
    }


    public function store(Request $request)
    {
        $producto = Producto::create([
            'producto' => $request->producto,
            'estadoProducto' => 1
        ]);

        return response()->json([
            'status' => true
        ]);
    }

    public function show(Producto $producto)
    {
        return $producto;
    }


    public function update(Request $request)
    {
        $producto = Producto::find($request->id);
        $producto->producto = $request->producto;
        $producto->estadoProducto = $request->estadoProducto;
        $producto->save();

        return response()->json([
            'status' => true
        ]);
    }

    public function destroy(Producto $producto)
    {
        $producto->delete();

        return response()->json([
            'status' => true
        ]);
    }
}
