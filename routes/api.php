<?php
namespace App\Http\Controllers;

use App\Models\Credito;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::post('login', [AuthController::class, 'login']);
Route::get('me', [AuthController::class, 'me']);

Route::group(['middleware' => ['jwt.verify']], function () {
    Route::get('empresa', [EmpresaController::class, 'index']);
});


Route::get('empresa', [EmpresaController::class, 'index']);
Route::post('empresa', [EmpresaController::class, 'update']);

Route::get('usuarios', [UserController::class, 'index']);
Route::post('usuarios', [UserController::class, 'store']);
Route::get('usuario/{user}', [UserController::class, 'getUser']);
Route::post('usuario', [UserController::class, 'update']);
Route::get('usuariod/{user}', [UserController::class, 'destroy']);
Route::post('usuario/clave', [UserController::class, 'password']);


Route::get('clientes', [ClienteController::class, 'index']);
Route::post('clientes', [ClienteController::class, 'store']);
Route::get('cliente/{cliente}', [ClienteController::class, 'show']);
Route::post('cliente', [ClienteController::class, 'update']);
Route::get('cliented/{cliente}', [ClienteController::class, 'destroy']);
Route::get('cliente/documento/{documento}', [ClienteController::class, 'validar']);
Route::get('cliente/detalle/{cliente}', [ClienteController::class, 'detalle']);

Route::get('productos', [ProductoController::class, 'index']);
Route::post('productos', [ProductoController::class, 'store']);
Route::get('producto/{producto}', [ProductoController::class, 'show']);
Route::post('producto', [ProductoController::class, 'update']);
Route::post('productod/{producto}', [ProductoController::class, 'destroy']);


Route::get('facturas', [FacturaController::class, 'index']);
Route::get('facturas/cliente/{cliente}', [FacturaController::class, 'facturas']);
Route::post('facturas', [FacturaController::class, 'store']);
Route::post('factura/final', [FacturaController::class, 'final']);
Route::get('factura/detalle/{factura}', [FacturaController::class, 'show']);

Route::post('relacion/store', [RelacionFacturaProductoController::class, 'store']);

Route::get('creditos', [CreditoController::class, 'index']);
Route::post('credito/simular', [CreditoController::class, 'simular']);
Route::post('credito/generar', [CreditoController::class, 'generar']);
Route::get('cuota/detalle/{cuota}', [CuotasCreditoController::class, 'index']);
Route::get('credito/detalle/{credito}', [CreditoController::class, 'detalle']);
Route::post('/cuota/update', [CuotasCreditoController::class, 'update']);
